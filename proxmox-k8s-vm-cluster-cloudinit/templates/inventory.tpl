[all]
%{ for host, ip in masters_ansible_ip ~}
${host} ansible_host=${ip} ip=${ip} etcd_member_name=${host}
%{ endfor ~}
%{ for host, ip in nodes_ansible_ip ~}
${host} ansible_host=${ip} ip=${ip}
%{ endfor ~}

[kube_control_plane]
%{ for host, ip in masters_ansible_ip ~}
${host}
%{ endfor ~}

[etcd]
%{ for host, ip in masters_ansible_ip ~}
${host}
%{ endfor ~}

[kube_node]
%{ for host, ip in nodes_ansible_ip ~}
${host} ansible_host=${ip} ip=${ip}
%{ endfor ~}

[calico_rr]

[k8s_cluster:children]
kube_control_plane
kube_node
calico_rr

[all:vars]
ansible_connexion=ssh
ansible_port=22
ansible_user=${ansible_user} 
ansible_ssh_private_key_file=${ansible_rsa}