terraform {
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = "2.9.10"
    }
  }
}

provider "proxmox" {
  # Configuration options
  pm_tls_insecure = true
  pm_api_url      = var.api_url
  pm_parallel     = 2
}

module "k8s-vm-cluster" {
  source = "../"

  # proxmox config
  target_node      = "lmart"
  proxmox_user     = var.proxmox_user
  proxmox_password = var.proxmox_password
  proxmox_host_ip  = var.proxmox_host_ip

  # vm config
  base_template = "ubuntu-cloud"
  agent         = 0
  onboot        = true

  # ansible config
  ansible_user                = var.ansible_user
  ansible_key_file            = var.ansible_key_file
  default_user_name           = "ansible"
  default_user_ssh_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHXe3XiDxxUDL9TylvssRVtAr1svmyIgJY96FdIl3Z7jNo5zdVJf80cAa+Ty1b7+5puboC0IH/ho/Trl6Zx4/+US2wX+TPlCTa2y8dvdWbZQGsJW0rojWO4hVh0ldw9aznDvAs4DFAaQeMloNIv16xTl1uvGPuc+BVNgkWBqqTh86PpSVBh1BtVrz7mGFwBu+2hPuSV8IEQGTt+XDjrrHO5vUG0H7j0tgrGmPYYtj5mR1pJF7Ns3tqjAuEjUQXBfY0sRZExwBcbykd43Pf1tZuGVu75X52D3OMRVOGrN2cSplNQysk9QxDdLxlajQwFIiZBHPt/1i9udb1367zMOLZ"
  run_ansible_kubespray_playbook = true

  # network config
  net_bridge = "vmbr0"
  net_model  = "virtio"
  cidr_mask  = "24"
  gateway    = "192.168.0.1"
  nameserver = "8.8.8.8"

  # loadbalancer config
  apisrv_domain   = "test.lmart.local"
  apisrv_port     = 8383
  lb_name         = "lb-node"
  lb_cores        = 1
  lb_sockets      = 1
  lb_memory       = 2048
  lb_os_storage   = "HDD-ZFS"
  lb_os_disk_size = "10G"
  lb_os_disk_type = "scsi"
  lb_additional_disks = [
    {
      storage = "HDD-ZFS"
      size    = "10G"
      type    = "scsi"
    },
  ]
  lb_ip_addr = "192.168.0.60"

  # masters config
  masters = {
    master01 = {
      name         = "master01-test-deploy",
      cores        = 2,
      sockets      = 2,
      memory       = 2048,
      ip_addr      = "192.168.0.61",
      os_storage   = "HDD-ZFS"
      os_disk_size = "10G"
      os_disk_type = "scsi"
      additional_disks = [
        {
          storage = "HDD-ZFS"
          size    = "10G"
          type    = "scsi"
        },
      ]
    },
    master02 = {
      name         = "master02-test-deploy",
      cores        = 2,
      sockets      = 2,
      memory       = 2048,
      ip_addr      = "192.168.0.62",
      os_storage   = "HDD-ZFS"
      os_disk_size = "10G"
      os_disk_type = "scsi"
      additional_disks = [
        {
          storage = "HDD-ZFS"
          size    = "10G"
          type    = "scsi"
        },
      ]
    },
  }

  # nodes config
  nodes = {
    node01 = {
      name         = "node01-test-deploy",
      cores        = 2,
      sockets      = 2,
      memory       = 2048,
      ip_addr      = "192.168.0.55",
      os_storage   = "HDD-ZFS"
      os_disk_size = "10G"
      os_disk_type = "scsi"
      additional_disks = [
        # Data disk
        {
          storage = "HDD-ZFS"
          size    = "10G"
          type    = "scsi"
        },
      ]
    },
    node02 = {
      name         = "node02-test-deploy",
      cores        = 2,
      sockets      = 2,
      memory       = 2048,
      ip_addr      = "192.168.0.56",
      os_storage   = "HDD-ZFS"
      os_disk_size = "100G"
      os_disk_type = "scsi"
      additional_disks = [
        # Data disk
        {
          storage = "HDD-ZFS"
          size    = "10G"
          type    = "scsi"
        },
      ]
    },
    node03 = {
      name         = "node03-test-deploy",
      cores        = 2,
      sockets      = 2,
      memory       = 2048,
      ip_addr      = "192.168.0.57",
      os_storage   = "HDD-ZFS"
      os_disk_size = "100G"
      os_disk_type = "scsi"
      additional_disks = [
        # Data disk
        {
          storage = "HDD-ZFS"
          size    = "10G"
          type    = "scsi"
        },
      ]
    },
  }
}

# resource "null_resource" "ansible-roles-bootstrap" {

#   triggers = {
#     always_run = "${timestamp()}"
#   }
#   provisioner "local-exec" {
#     # Bootstrap script called with private_ip of each node in the clutser
#     command     = "ansible-galaxy install -p roles -r requirements.yml"
#     working_dir = "ansible"
#   }
# }

# resource "null_resource" "ansible-run" {
#   depends_on = [
#     null_resource.ansible-roles-bootstrap,
#     module.k8s-vm-cluster
#   ]
#   triggers = {
#     always_run = "${timestamp()}"
#   }
#   provisioner "local-exec" {

#     command     = "ansible-playbook -i inventory additional.yml"
#     working_dir = "ansible"
#   }
# }
