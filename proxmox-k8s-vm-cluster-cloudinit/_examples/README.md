# _examples

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_proxmox"></a> [proxmox](#requirement\_proxmox) | 2.9.10 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_null"></a> [null](#provider\_null) | 3.1.1 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_k8s-vm-cluster"></a> [k8s-vm-cluster](#module\_k8s-vm-cluster) | ../ | n/a |

## Resources

| Name | Type |
|------|------|
| [null_resource.ansible-roles-bootstrap](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.ansible-run](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ansible_key_file"></a> [ansible\_key\_file](#input\_ansible\_key\_file) | n/a | `string` | n/a | yes |
| <a name="input_ansible_user"></a> [ansible\_user](#input\_ansible\_user) | n/a | `string` | n/a | yes |
| <a name="input_api_url"></a> [api\_url](#input\_api\_url) | n/a | `string` | n/a | yes |
| <a name="input_proxmox_host_ip"></a> [proxmox\_host\_ip](#input\_proxmox\_host\_ip) | n/a | `string` | n/a | yes |
| <a name="input_proxmox_password"></a> [proxmox\_password](#input\_proxmox\_password) | n/a | `string` | n/a | yes |
| <a name="input_proxmox_user"></a> [proxmox\_user](#input\_proxmox\_user) | n/a | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
