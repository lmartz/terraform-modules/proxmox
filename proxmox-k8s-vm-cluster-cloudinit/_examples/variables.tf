variable "proxmox_user" {
  type      = string
  sensitive = true
}
variable "proxmox_password" {
  type      = string
  sensitive = true
}
variable "proxmox_host_ip" {
  type      = string
  sensitive = true
}
variable "api_url" {
  type = string
}
variable "ansible_user" {
  type      = string
  sensitive = true
}
variable "ansible_key_file" {
  type      = string
  sensitive = true
}
