variable "proxmox_user" {
  type      = string
  sensitive = true
}
variable "proxmox_password" {
  type      = string
  sensitive = true
}
variable "proxmox_host_ip" {
  type      = string
  sensitive = true
}
variable "target_node" {
  type = string
}
variable "net_bridge" {
  type = string
}
variable "net_model" {
  type = string
}
variable "cidr_mask" {
  type = string
}
variable "gateway" {
  type = string
}
variable "nameserver" {
  type = string
}
variable "base_template" {
  type = string
}
# variable "lb_name" {
#   type    = string
# }
# variable "lb_os_storage" {
#   type = string
# }
# variable "lb_os_disk_size" {
#   type = string
# }
# variable "lb_os_disk_type" {
#   type = string
# }
# variable "lb_additional_disks" {
#   type = list(map(any))
# }
# variable "lb_ip_addr" {
#   type    = string
# }
# variable "lb_min_mem_balloon" {
#   type    = number
#   default = null
# }
# variable "lb_cores" {
#   type    = number
# }
# variable "lb_sockets" {
#   type    = number
# }
# variable "lb_memory" {
#   type    = number
# }
variable "agent" {
  type = string
}
variable "onboot" {
  type = bool
}
variable "masters" {
  type = map(object({
    name             = string,
    cores            = number,
    sockets          = number,
    memory           = number,
    additional_disks = list(map(any)),
    ip_addr          = string,
    os_storage       = string,
    os_disk_size     = string,
    os_disk_type     = string
  }))
  default = {
    "master-node" = {
      cores            = 1
      additional_disks = []
      ip_addr          = ""
      memory           = 2048
      name             = "k8s-master-node-default"
      sockets          = 2
      os_storage       = "local-lvm",
      os_disk_size     = "50GB",
      os_disk_type     = "scsi"
    }
  }
}
variable "masters_desc" {
  type    = string
  default = "master_node"
}
variable "masters_min_mem_balloon" {
  type    = number
  default = 0
}
variable "nodes" {
  type = map(object({
    name             = string,
    cores            = number,
    sockets          = number,
    memory           = number,
    additional_disks = list(map(any)),
    ip_addr          = string,
    os_storage       = string,
    os_disk_size     = string,
    os_disk_type     = string
  }))
  default = {
    "worker-node" = {
      cores            = 1
      additional_disks = []
      ip_addr          = ""
      memory           = 2048
      name             = "k8s-worker-node-default"
      sockets          = 2
      os_storage       = "local-lvm",
      os_disk_size     = "50GB",
      os_disk_type     = "scsi"
    }
  }
}
variable "nodes_desc" {
  type    = string
  default = "worker_node"
}
variable "nodes_min_mem_balloon" {
  type    = number
  default = 0
}
variable "default_user_name" {
  type = string
}
variable "default_user_shell" {
  type    = string
  default = "/bin/bash"
}
variable "default_user_ssh_public_key" {
  type = string
}
variable "additionnal_users" {
  type = map(object({
    name           = string,
    shell          = string,
    have_privs     = bool,
    authorized_key = string,
  }))
  default = {
  }
}
variable "ansible_user" {
  type      = string
  sensitive = true
}
variable "ansible_key_file" {
  type      = string
  sensitive = true
}

variable "ansible_inventory_generation_path" {
  type = string
}