terraform {
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = "~> 2.9.0"
    }
  }
}

locals {
  proxmox_conf = {
    target_node      = var.target_node
    proxmox_user     = var.proxmox_user
    proxmox_password = var.proxmox_password
    proxmox_host_ip  = var.proxmox_host_ip
  }
  network_conf = {
    net_bridge = var.net_bridge
    net_model  = var.net_model
    gateway    = var.gateway
    cidr_mask  = var.cidr_mask
    nameserver = var.nameserver
  }
  hosts_conf = {
    default_user_name           = var.default_user_name
    default_user_shell          = var.default_user_shell
    default_user_ssh_public_key = var.default_user_ssh_public_key
  }
}

module "masters" {
  for_each = var.masters

  source          = "../proxmox-vm-cloudinit"
  vm_name         = each.value.name
  vm_desc         = var.masters_desc
  target_node     = local.proxmox_conf.target_node
  source_clone    = var.base_template
  agent           = var.agent
  cores           = each.value.cores
  sockets         = each.value.sockets
  memory          = each.value.memory
  min_mem_balloon = var.masters_min_mem_balloon
  onboot          = true
  net_bridge      = local.network_conf.net_bridge
  net_model       = local.network_conf.net_model
  disks = concat(
    [
      {
        storage = each.value.os_storage
        size    = each.value.os_disk_size
        type    = each.value.os_disk_type
      },
    ],
    each.value.additional_disks
  )
  ip_addr    = each.value.ip_addr
  cidr_mask  = local.network_conf.cidr_mask
  gateway    = local.network_conf.gateway
  nameserver = local.network_conf.nameserver

  additionnal_users           = var.additionnal_users
  proxmox_user                = local.proxmox_conf.proxmox_user
  proxmox_password            = local.proxmox_conf.proxmox_password
  proxmox_host_ip             = local.proxmox_conf.proxmox_host_ip
  default_user_name           = local.hosts_conf.default_user_name
  default_user_shell          = local.hosts_conf.default_user_shell
  default_user_ssh_public_key = local.hosts_conf.default_user_ssh_public_key

}

module "nodes" {
  for_each = var.nodes

  source          = "../proxmox-vm-cloudinit"
  vm_name         = each.value.name
  vm_desc         = var.nodes_desc
  target_node     = local.proxmox_conf.target_node
  source_clone    = var.base_template
  agent           = var.agent
  cores           = each.value.cores
  sockets         = each.value.sockets
  memory          = each.value.memory
  min_mem_balloon = var.nodes_min_mem_balloon
  onboot          = true
  net_bridge      = local.network_conf.net_bridge
  net_model       = local.network_conf.net_model
  disks = concat(
    [
      {
        storage = each.value.os_storage
        size    = each.value.os_disk_size
        type    = each.value.os_disk_type
      },
    ],
    each.value.additional_disks
  )
  ip_addr    = each.value.ip_addr
  cidr_mask  = local.network_conf.cidr_mask
  gateway    = local.network_conf.gateway
  nameserver = local.network_conf.nameserver

  additionnal_users           = var.additionnal_users
  proxmox_user                = local.proxmox_conf.proxmox_user
  proxmox_password            = local.proxmox_conf.proxmox_password
  proxmox_host_ip             = local.proxmox_conf.proxmox_host_ip
  default_user_name           = local.hosts_conf.default_user_name
  default_user_shell          = local.hosts_conf.default_user_shell
  default_user_ssh_public_key = local.hosts_conf.default_user_ssh_public_key
}

###########
# Ansible Conf
###########
resource "local_file" "inventory" {
  content = templatefile("${path.module}/templates/inventory.tpl",
    {
      masters_ansible_ip                 = { for key, vm in module.masters : key => vm.ip }
      nodes_ansible_ip                   = { for key, vm in module.nodes : key => vm.ip }
      ansible_user                       = var.ansible_user
      ansible_rsa                        = var.ansible_key_file
    }
  )
  filename = var.ansible_inventory_generation_path
}


# resource "null_resource" "ansible-roles-bootstrap" {

#   triggers = {
#     always_run = "${timestamp()}"
#   }
#   provisioner "local-exec" {
#     # Bootstrap script called with private_ip of each node in the clutser
#     command     = "ansible-galaxy install -p roles -r requirements.yml"
#     working_dir = "${path.module}/ansible"
#   }
# }

# resource "null_resource" "ansible-run" {
#   depends_on = [
#     local_file.inventory,
#     module.lb,
#     module.masters,
#     module.nodes
#   ]
#   count = var.run_ansible_kubespray_playbook ? 1 : 0
#   triggers = {
#     always_run = "${timestamp()}"
#   }
#   provisioner "local-exec" {

#     command     = "ansible-playbook -i kubespray/inventory/lmartio/inventory.ini main.yml -e \"lb_ip=${module.lb.ip} lb_frontend_port=${var.apisrv_port}\" && ansible-playbook -i kubespray/inventory/lmartio/inventory.ini --become --become-user=root kubespray/cluster.yml -e kube_version=${var.kube_version} -e apiserver_loadbalancer_domain_name=${var.apisrv_domain} -e '{\"loadbalancer_apiserver\" : {\"address\" :\"${module.lb.ip}\", \"port\":\"${var.apisrv_port}\"}}'"
#     working_dir = "${path.module}/ansible"
#   }
# }