# proxmox-k8s-vm-cluster-cloudinit

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_proxmox"></a> [proxmox](#requirement\_proxmox) | 2.9.10 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_local"></a> [local](#provider\_local) | 2.2.3 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.1.1 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_lb"></a> [lb](#module\_lb) | ../proxmox-vm-cloudinit | n/a |
| <a name="module_masters"></a> [masters](#module\_masters) | ../proxmox-vm-cloudinit | n/a |
| <a name="module_nodes"></a> [nodes](#module\_nodes) | ../proxmox-vm-cloudinit | n/a |

## Resources

| Name | Type |
|------|------|
| [local_file.inventory](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [null_resource.ansible-roles-bootstrap](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.ansible-run](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_additionnal_users"></a> [additionnal\_users](#input\_additionnal\_users) | n/a | <pre>map(object({<br>    name           = string,<br>    shell          = string,<br>    have_privs     = bool,<br>    authorized_key = string,<br>  }))</pre> | `{}` | no |
| <a name="input_agent"></a> [agent](#input\_agent) | n/a | `string` | n/a | yes |
| <a name="input_ansible_key_file"></a> [ansible\_key\_file](#input\_ansible\_key\_file) | n/a | `string` | n/a | yes |
| <a name="input_ansible_user"></a> [ansible\_user](#input\_ansible\_user) | n/a | `string` | n/a | yes |
| <a name="input_base_template"></a> [base\_template](#input\_base\_template) | n/a | `string` | n/a | yes |
| <a name="input_cidr_mask"></a> [cidr\_mask](#input\_cidr\_mask) | n/a | `string` | n/a | yes |
| <a name="input_default_user_name"></a> [default\_user\_name](#input\_default\_user\_name) | n/a | `string` | n/a | yes |
| <a name="input_default_user_shell"></a> [default\_user\_shell](#input\_default\_user\_shell) | n/a | `string` | `"/bin/bash"` | no |
| <a name="input_default_user_ssh_public_key"></a> [default\_user\_ssh\_public\_key](#input\_default\_user\_ssh\_public\_key) | n/a | `string` | n/a | yes |
| <a name="input_deploy_lb"></a> [deploy\_lb](#input\_deploy\_lb) | n/a | `bool` | `false` | no |
| <a name="input_gateway"></a> [gateway](#input\_gateway) | n/a | `string` | n/a | yes |
| <a name="input_lb_cores"></a> [lb\_cores](#input\_lb\_cores) | n/a | `number` | `null` | no |
| <a name="input_lb_disks"></a> [lb\_disks](#input\_lb\_disks) | n/a | `list(any)` | `[]` | no |
| <a name="input_lb_ip_addr"></a> [lb\_ip\_addr](#input\_lb\_ip\_addr) | n/a | `string` | `null` | no |
| <a name="input_lb_memory"></a> [lb\_memory](#input\_lb\_memory) | n/a | `number` | `null` | no |
| <a name="input_lb_min_mem_balloon"></a> [lb\_min\_mem\_balloon](#input\_lb\_min\_mem\_balloon) | n/a | `number` | `null` | no |
| <a name="input_lb_sockets"></a> [lb\_sockets](#input\_lb\_sockets) | n/a | `number` | `null` | no |
| <a name="input_lb_vm_desc"></a> [lb\_vm\_desc](#input\_lb\_vm\_desc) | n/a | `string` | `null` | no |
| <a name="input_lb_vm_name"></a> [lb\_vm\_name](#input\_lb\_vm\_name) | n/a | `string` | `null` | no |
| <a name="input_masters"></a> [masters](#input\_masters) | n/a | <pre>map(object({<br>    name             = string,<br>    cores            = number,<br>    sockets          = number,<br>    memory           = number,<br>    additional_disks = list(map(any)),<br>    ip_addr          = string,<br>    os_storage       = string,<br>    os_disk_size     = string,<br>    os_disk_type     = string<br>  }))</pre> | <pre>{<br>  "master-node": {<br>    "additional_disks": [],<br>    "cores": 1,<br>    "ip_addr": "",<br>    "memory": 2048,<br>    "name": "k8s-master-node-default",<br>    "os_disk_size": "50GB",<br>    "os_disk_type": "scsi",<br>    "os_storage": "local-lvm",<br>    "sockets": 2<br>  }<br>}</pre> | no |
| <a name="input_masters_desc"></a> [masters\_desc](#input\_masters\_desc) | n/a | `string` | `"master_node"` | no |
| <a name="input_masters_min_mem_balloon"></a> [masters\_min\_mem\_balloon](#input\_masters\_min\_mem\_balloon) | n/a | `number` | `null` | no |
| <a name="input_nameserver"></a> [nameserver](#input\_nameserver) | n/a | `string` | n/a | yes |
| <a name="input_net_bridge"></a> [net\_bridge](#input\_net\_bridge) | n/a | `string` | n/a | yes |
| <a name="input_net_model"></a> [net\_model](#input\_net\_model) | n/a | `string` | n/a | yes |
| <a name="input_nodes"></a> [nodes](#input\_nodes) | n/a | <pre>map(object({<br>    name             = string,<br>    cores            = number,<br>    sockets          = number,<br>    memory           = number,<br>    additional_disks = list(map(any)),<br>    ip_addr          = string,<br>    os_storage       = string,<br>    os_disk_size     = string,<br>    os_disk_type     = string<br><br><br>  }))</pre> | <pre>{<br>  "worker-node": {<br>    "additional_disks": [],<br>    "cores": 1,<br>    "ip_addr": "",<br>    "memory": 2048,<br>    "name": "k8s-worker-node-default",<br>    "os_disk_size": "50GB",<br>    "os_disk_type": "scsi",<br>    "os_storage": "local-lvm",<br>    "sockets": 2<br>  }<br>}</pre> | no |
| <a name="input_nodes_desc"></a> [nodes\_desc](#input\_nodes\_desc) | n/a | `string` | `null` | no |
| <a name="input_nodes_min_mem_balloon"></a> [nodes\_min\_mem\_balloon](#input\_nodes\_min\_mem\_balloon) | n/a | `number` | `null` | no |
| <a name="input_onboot"></a> [onboot](#input\_onboot) | n/a | `bool` | n/a | yes |
| <a name="input_proxmox_host_ip"></a> [proxmox\_host\_ip](#input\_proxmox\_host\_ip) | n/a | `string` | n/a | yes |
| <a name="input_proxmox_password"></a> [proxmox\_password](#input\_proxmox\_password) | n/a | `string` | n/a | yes |
| <a name="input_proxmox_user"></a> [proxmox\_user](#input\_proxmox\_user) | n/a | `string` | n/a | yes |
| <a name="input_target_node"></a> [target\_node](#input\_target\_node) | n/a | `string` | n/a | yes |
| <a name="input_user-defined-ansible-cluster-group"></a> [user-defined-ansible-cluster-group](#input\_user-defined-ansible-cluster-group) | n/a | `string` | `"cluster"` | no |
| <a name="input_user-defined-ansible-lb-group"></a> [user-defined-ansible-lb-group](#input\_user-defined-ansible-lb-group) | n/a | `string` | `"lb"` | no |
| <a name="input_user-defined-ansible-masters-group"></a> [user-defined-ansible-masters-group](#input\_user-defined-ansible-masters-group) | n/a | `string` | `"masters"` | no |
| <a name="input_user-defined-ansible-workers-group"></a> [user-defined-ansible-workers-group](#input\_user-defined-ansible-workers-group) | n/a | `string` | `"workers"` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
