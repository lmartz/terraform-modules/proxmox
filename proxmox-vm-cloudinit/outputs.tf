output "ip" {
  value = var.ip_addr
}
output "name" {
  value = proxmox_vm_qemu.resource-name.name
}
