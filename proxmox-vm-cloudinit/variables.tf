variable "vm_name" {
  type = string
}
variable "vm_desc" {
  type = string
}
variable "target_node" {
  type = string
}
variable "source_clone" {
  type = string
}
variable "agent" {
  type = string
}
variable "cores" {
  type = number
}
variable "sockets" {
  type = number
}
variable "memory" {
  type = number
}
variable "onboot" {
  type = bool
}
variable "net_bridge" {
  type = string
}
variable "net_model" {
  type = string
}
variable "ip_addr" {
  type = string
}
variable "cidr_mask" {
  type = string
}
variable "gateway" {
  type = string
}
variable "nameserver" {
  type = string
}
variable "packages_list" {
  type    = list(string)
  default = ["qemu-guest-agent"]
}
variable "default_user_name" {
  type = string
}
variable "default_user_shell" {
  type    = string
  default = "/bin/bash"
}
variable "default_user_ssh_public_key" {
  type = string
}

variable "additionnal_users" {
  type = map(object({
    name           = string,
    shell          = string,
    have_privs     = bool,
    authorized_key = string,
  }))
  default = {
  }
}

variable "proxmox_user" {
  type      = string
  sensitive = true
}
variable "proxmox_password" {
  type      = string
  sensitive = true
}
variable "proxmox_host_ip" {
  type      = string
  sensitive = true
}
variable "disks" {
  type    = list(any)
  default = []
}
variable "disk_default_type" {
  type    = string
  default = "scsi"

  validation {
    condition     = contains(["scsi", "sata", "virtio", "ide"], var.disk_default_type)
    error_message = "The Proxmox disk type is invalid."
  }
}

variable "disk_default_size" {
  type    = string
  default = "10G"
}

variable "disk_default_storage" {
  type    = string
  default = "local-lvm"
}

variable "ansible_provisionning" {
  type    = bool
  default = false
}
variable "user_defined_ansible_group_name" {
  type    = string
  default = null
}
variable "playbook_name" {
  type    = string
  default = null
}
variable "ansible_user" {
  type    = string
  default = null
}
variable "ansible_key_file" {
  type    = string
  default = null
}
variable "ansible_bootstrap_roles" {
  type    = bool
  default = false
}
variable "use_custom_inventory" {
  type    = bool
  default = false
}
variable "custom_inventory_name" {
  type    = string
  default = null
}
variable "min_mem_balloon" {
  type    = number
  default = 0
}
