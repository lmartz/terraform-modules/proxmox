terraform {
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = "2.9.10"
    }
  }
}

resource "proxmox_vm_qemu" "resource-name" {
  # Vm conf
  name        = var.vm_name
  desc        = var.vm_desc
  target_node = var.target_node
  clone       = var.source_clone
  agent       = var.agent
  cores       = var.cores
  sockets     = var.sockets
  cpu         = "host"
  memory      = var.memory
  onboot      = var.onboot
  balloon     = var.min_mem_balloon
  os_type     = "cloud-init"

  # Disks
  dynamic "disk" {
    for_each = var.disks
    iterator = curr_disk
    content {
      type    = lookup(curr_disk.value, "type", var.disk_default_type)
      storage = lookup(curr_disk.value, "storage", var.disk_default_storage)
      size    = lookup(curr_disk.value, "size", var.disk_default_size)
    }
  }

  # Network conf
  network {
    bridge = var.net_bridge
    model  = var.net_model
  }
  ipconfig0  = "ip=${var.ip_addr}/${var.cidr_mask},gw=${var.gateway}"
  nameserver = var.nameserver

  # Cloudinit conf
  cicustom = "user=local:snippets/${element(split("/", local_file.cloud_init_user_data_file.filename), length(split("/", local_file.cloud_init_user_data_file.filename)) - 1)}"
}

# This resource renders a yaml cloud init conf
resource "local_file" "cloud_init_user_data_file" {
  content = templatefile(
    "${path.module}/templates/user_data.tftpl",
    {
      default_user_name           = var.default_user_name
      default_user_shell          = var.default_user_shell
      default_user_ssh_public_key = var.default_user_ssh_public_key
      hostname                    = var.vm_name
      packages                    = var.packages_list
      users                       = var.additionnal_users
    }
  )
  filename = "${path.root}/${var.vm_name}_user_data.yml"
}

# This resource upload the yaml cloud init conf
resource "null_resource" "upload_cloud_init_generated_file" {
  connection {
    type     = "ssh"
    user     = var.proxmox_user
    password = var.proxmox_password
    host     = var.proxmox_host_ip
  }

  provisioner "file" {
    source      = local_file.cloud_init_user_data_file.filename
    destination = "/var/lib/vz/snippets/${element(split("/", local_file.cloud_init_user_data_file.filename), length(split("/", local_file.cloud_init_user_data_file.filename)) - 1)}"
  }
}


resource "local_file" "inventory" {
  count = var.ansible_provisionning && var.use_custom_inventory == false ? 1 : 0
  content = templatefile("${path.module}/templates/inventory.tftpl",
    {
      user_defined_ansible_group_name = var.user_defined_ansible_group_name
      host                            = var.vm_name
      ip                              = var.ip_addr
      ansible_user                    = var.ansible_user
      ansible_rsa                     = var.ansible_key_file
      ansible_port                    = "22"
      ansible_connection              = "ssh"
    }
  )
  filename = "${path.root}/ansible/inventory"
}

resource "null_resource" "ansible-roles-bootstrap" {
  count = var.ansible_provisionning && var.ansible_bootstrap_roles ? 1 : 0
  triggers = {
    always_run = "${timestamp()}"
  }
  provisioner "local-exec" {
    # Bootstrap script called with private_ip of each node in the clutser
    command     = "ansible-galaxy install -p roles -r requirements.yml"
    working_dir = "${path.root}/ansible"
  }
}

resource "null_resource" "ansible-run" {
  count = var.ansible_provisionning ? 1 : 0
  depends_on = [
    local_file.inventory,
    null_resource.ansible-roles-bootstrap,
    proxmox_vm_qemu.resource-name
  ]
  triggers = {
    always_run = "${timestamp()}"
  }
  provisioner "local-exec" {

    command     = var.use_custom_inventory ? "ansible-playbook -i ${var.custom_inventory_name} ${var.playbook_name}" : "ansible-playbook -i inventory ${var.playbook_name}"
    working_dir = "${path.root}/ansible"
  }
}
