output "ip" {
  value = module.proxmox-vm-cloudinit.ip
}
output "name" {
  value = module.proxmox-vm-cloudinit.name
}
