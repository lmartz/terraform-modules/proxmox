terraform {
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = "2.9.10"
    }
  }
}
provider "proxmox" {
  # Configuration options
  pm_tls_insecure = true
  pm_api_url      = var.api_url
}

module "proxmox-vm-cloudinit" {

  source = "../"

  vm_name         = "test-deploy"
  vm_desc         = "test"
  target_node     = "lmart"
  source_clone    = "ubuntu-cloud"
  agent           = 0
  cores           = 2
  sockets         = 2
  memory          = 2048
  min_mem_balloon = 0
  onboot          = true
  net_bridge      = "vmbr0"
  net_model       = "virtio"

  # Disks
  disks = [
    # OS disk
    {
      storage = "HDD-ZFS"
      size    = "50G"
      type    = "scsi"
    },
    # Data disk
    {
      storage = "HDD-ZFS"
      size    = "50G"
      type    = "scsi"
    },
  ]

  ip_addr                     = "192.168.0.186"
  cidr_mask                   = "24"
  gateway                     = "192.168.0.1"
  nameserver                  = "8.8.8.8"
  packages_list               = ["qemu-guest-agent"]
  default_user_name           = "ansible"
  default_user_ssh_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHXe3XiDxxUDL9TylvssRVtAr1svmyIgJY96FdIl3Z7jNo5zdVJf80cAa+Ty1b7+5puboC0IH/ho/Trl6Zx4/+US2wX+TPlCTa2y8dvdWbZQGsJW0rojWO4hVh0ldw9aznDvAs4DFAaQeMloNIv16xTl1uvGPuc+BVNgkWBqqTh86PpSVBh1BtVrz7mGFwBu+2hPuSV8IEQGTt+XDjrrHO5vUG0H7j0tgrGmPYYtj5mR1pJF7Ns3tqjAuEjUQXBfY0sRZExwBcbykd43Pf1tZuGVu75X52D3OMRVOGrN2cSplNQysk9QxDdLxlajQwFIiZBHPt/1i9udb1367zMOLZ"
  additionnal_users = {

    user2 = {
      name           = "user2",
      shell          = "/bin/sh",
      have_privs     = false,
      authorized_key = "ssh-rsa dummydata"
    }
  }
  proxmox_user     = var.proxmox_user
  proxmox_password = var.proxmox_password
  proxmox_host_ip  = var.proxmox_host_ip

  ansible_provisionning           = true
  ansible_bootstrap_roles         = true
  user_defined_ansible_group_name = "ubuntu"
  playbook_name                   = "main.yml"
  ansible_user                    = "ansible"
  ansible_key_file                = var.ansible_key_file
}
